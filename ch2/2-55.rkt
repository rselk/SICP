#lang racket

;; Exercise 2.55.  Eva Lu Ator types to the interpreter the expression

;; (car ''abracadabra)

;; To her surprise, the interpreter prints back quote. Explain.

(car ''abracadabra)

;; not a surprise. 
;; car (quote (quote etc))
;; this will return `quote